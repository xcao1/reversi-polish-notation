// ===========================
// Reversi Polish Notation
// Xinlei Cao
// CS570
// ===========================

// head files
#include <iostream>
#include <fstream>
#include <string>
#include <cctype>
#include <cmath>
#include <iomanip>
// using namespace
using namespace std;



// ============================================
// main function
int main()
{
	// ========================================
	// open file
	// store the input file path into a string
	string inputFilePath;
	cout<<"Please enter the path to the input file :"<<endl;
	cin>>inputFilePath; 

	cout<<"\nStore \""<<inputFilePath<<"|( cstring version :"<<inputFilePath.c_str()<<")\" as path to file.\n"<<endl;

	// open the file with the path
	ifstream mInFile(inputFilePath.c_str());
	if( mInFile.fail())
	{
		cout<<"Can not find the file with the given path:\n"<<inputFilePath<< endl;	
	}
	else
	{
		cout<<"Open file successfully with the given path:\n"<<inputFilePath<<endl;

		// ===============================
		// Reversi Polish Notation
		// used to get every character from file
		char sym;
		// store not more than three numbers
		double mArray[5] = { 0, 0, 0, 0, 0 };
		// if the number is double
		bool isDouble = false;
		// the precise of the double number
		int precise = 0;
		// count which number to store the digit
		int count = 0;
		// if the last character is a digit,default value is false
		bool wasLastADigit = false;
		// if syntax error
		bool isSyntaxError = false;
		// if pow operator
		bool wasLastAP = false, wasLastAO = false;
		// read until end-of-file
	
	
		while( !mInFile.eof() )
		{
			// read every character from file
			sym = mInFile.get();
		
			// the count should be in range 0 - 3
			if( count < 0 || count > 5 )
				cerr<<"\n(error:count should not smaller than 0 or bigger than 5)";
			// store every number
			if( isdigit( sym ) )
			{// if the character readed is a digit
				wasLastADigit = true;
				// store the digit into right number
				double digit = sym - '0';
				if( isDouble == false )
				{
					switch( count )
					{
					case 0:// if there is no number
						mArray[0] = mArray[0] * 10 + digit;
						break;
					case 1:
						mArray[1] = mArray[1] * 10 + digit;
						break;
					case 2:
						mArray[2] = mArray[2] * 10 + digit;
						break;
					case 3:
						mArray[3] = mArray[3] * 10 + digit;
						break;
					case 4:
						mArray[4] = mArray[4] * 10 + digit;
						break;
					default:
						// error
						isSyntaxError = true;
						cerr<<"\n(error : count should only be 0,1,2,3,4,here .)";
						break;
					}
				}
				else
				{
					precise ++;// defualt 0
					digit = digit * pow( 10.0 , -1 * precise );
					switch( count )
					{
					case 0:
						mArray[0] = mArray[0] + digit;
						break;
					case 1:
						mArray[1] = mArray[1] + digit;
						break;
					case 2:
						mArray[2] = mArray[2] + digit;
						break;
					case 3:
						mArray[3] = mArray[3] + digit;
						break;
					case 4:
						mArray[4] = mArray[4] + digit;
						break;
					default:
						// error
						isSyntaxError = true;
						cerr<<"\n(error : count should only be 0,1,2,3,4,here .)";
						break;
					}
				}
			}
			else
			{
				if( sym != '.' )
				{
					// set the count , reset wasLastADigit
					if( wasLastADigit == true )
					{
						count++;
						isDouble = false;
						precise = 0;
					}
					if( count > 5 )
						cerr<<"\n(error : count should not bigger than 5 .)";
					wasLastADigit = false;
				}
				else
				{
					isDouble = true;
					precise = 0;
				}

				// if reach the end of line
				if( sym == '\n' )
				{// if it is the end of the line
					// print the result
					if( isSyntaxError == true )
						cout<<"\nSYNTAX ERROR"<<endl;
					else
					{
						cout<<setiosflags(ios::fixed|ios::showpoint)<<setprecision(4);
						if( count == 1 )
							cout<<"\nResult :"<<mArray[0]<<endl;
						else if( count == 0 )
							cout<<"\n";
						else
						{
							cerr<<"\n( error : while reach the end of the line,the count should either be 0 or 1 .)";
							cout<<"\nSYNTAX ERROR"<<endl;
						}
					}// reset the formulation
					count = 0;
					for( int i = 0; i != 5; i++ )
					{
						mArray[i] = 0;
					}
					wasLastADigit = false;
					isSyntaxError = false;
					wasLastAP = false;
					wasLastAO = false;
					isDouble = false;
					precise = 0;
				}
				// =========================================
				// calculation with operators
				// +
				if( sym == '+' )
				{
					switch( count )
					{
					case 5:
						mArray[3] = mArray[3] + mArray[4];
						for( int i = 4; i != 5; i++ )
						{
							mArray[i] = 0;
						}
						count --;
						break;
					case 4:
						mArray[2] = mArray[2] + mArray[3];
						for( int i = 3; i != 5; i++ )
						{
							mArray[i] = 0;
						}
						count --;
						break;
					case 3:
						mArray[1] = mArray[1] + mArray[2];
						for( int i = 2; i != 5; i++ )
						{
							mArray[i] = 0;
						}
						count --;
						break;
					case 2:
						mArray[0] = mArray[0] + mArray[1];
						for( int i = 1; i != 5; i++ )
						{
							mArray[i] = 0;
						}
						count --;
						break;
					default :
						// error
						isSyntaxError = true;
						cerr<<"\n( error : more than 5 numbers or less than 2 numbers . )";
						break;
					}
				}
				else if( sym == '-' )
				{
					switch( count )
					{
					case 5:
						mArray[3] = mArray[3] - mArray[4];
						for( int i = 4; i != 5; i++ )
						{
							mArray[i] = 0;
						}
						count --;
						break;
					case 4:
						mArray[2] = mArray[2] - mArray[3];
						for( int i = 3; i != 5; i++ )
						{
							mArray[i] = 0;
						}
						count --;
						break;
					case 3:
						mArray[1] = mArray[1] - mArray[2];
						for( int i = 2; i != 5; i++ )
						{
							mArray[i] = 0;
						}
						count --;
						break;
					case 2:
						mArray[0] = mArray[0] - mArray[1];
						for( int i = 1; i != 5; i++ )
						{
							mArray[i] = 0;
						}
						count --;
						break;
					default :
						// error
						isSyntaxError = true;
						cerr<<"\n( error : more than 5 numbers or less than 2 numbers . )";
						break;
					}
				}
				else if( sym == '*' )
				{
					switch( count )
					{
					case 5:
						mArray[3] = mArray[3] * mArray[4];
						for( int i = 4; i != 5; i++ )
						{
							mArray[i] = 0;
						}
						count --;
						break;
					case 4:
						mArray[2] = mArray[2] * mArray[3];
						for( int i = 3; i != 5; i++ )
						{
							mArray[i] = 0;
						}
						count --;
						break;
					case 3:
						mArray[1] = mArray[1] * mArray[2];
						for( int i = 2; i != 5; i++ )
						{
							mArray[i] = 0;
						}
						count --;
						break;
					case 2:
						mArray[0] = mArray[0] * mArray[1];
						for( int i = 1; i != 5; i++ )
						{
							mArray[i] = 0;
						}
						count --;
						break;
					default :
						// error
						isSyntaxError = true;
						cerr<<"\n( error : more than 5 numbers or less than 2 numbers . )";
						break;
					}
				}
				else if( sym == '/' )
				{
					switch( count )
					{
					case 5:
						mArray[3] = mArray[3] / mArray[4];
						for( int i = 4; i != 5; i++ )
						{
							mArray[i] = 0;
						}
						count --;
						break;
					case 4:
						mArray[2] = mArray[2] / mArray[3];
						for( int i = 3; i != 5; i++ )
						{
							mArray[i] = 0;
						}
						count --;
						break;
					case 3:
						mArray[1] = mArray[1] / mArray[2];
						for( int i = 2; i != 5; i++ )
						{
							mArray[i] = 0;
						}
						count --;
						break;
					case 2:
						mArray[0] = mArray[0] / mArray[1];
						for( int i = 1; i != 5; i++ )
						{
							mArray[i] = 0;
						}
						count --;
						break;
					default :
						// error
						isSyntaxError = true;
						cerr<<"\n( error : more than 5 numbers or less than 2 numbers . )";
						break;
					}
				}
				else if( sym == '%' )
				{
					int a,b;
					switch( count )
					{
					case 5:
						a = mArray[3];
						b = mArray[4];
						mArray[3] = a % b;
						for( int i = 4; i != 5; i++ )
						{
							mArray[i] = 0;
						}
						count --;
						break;
					case 4:
						a = mArray[2];
						b = mArray[3];
						mArray[2] = a % b;
						for( int i = 3; i != 5; i++ )
						{
							mArray[i] = 0;
						}
						count --;
						break;
					case 3:
						a = mArray[1];
						b = mArray[2];
						mArray[1] = a % b;
						for( int i = 2; i != 5; i++ )
						{
							mArray[i] = 0;
						}
						count --;
						break;
					case 2:
						a = mArray[0];
						b = mArray[1];
						mArray[0] = a % b;
						for( int i = 1; i != 5; i++ )
						{
							mArray[i] = 0;
						}
						count --;
						break;
					default :
						// error
						isSyntaxError = true;
						cerr<<"\n( error : more than 5 numbers or less than 2 numbers . )";
						break;
					}
				}
				else // pow or '_', '\t','\n',eof or error
				{
					// pow
					if( sym == 'p' )
					{
						wasLastAP = true;
					}
					else if( sym == 'o' )
					{
						if( wasLastAP == true )
							wasLastAO = true;
						wasLastAP = false;
					}
					else if( sym == 'w' )
					{
						if( wasLastAO == true )
						{// the pow operator
							switch( count )
							{
							case 5:
								mArray[3] = pow( mArray[3] , mArray[4] );
								for( int i = 4; i != 5; i++ )
								{
									mArray[i] = 0;
								}
								count --;
								break;
							case 4:
								mArray[2] = pow( mArray[2] , mArray[3] );
								for( int i = 3; i != 5; i++ )
								{
									mArray[i] = 0;
								}
								count --;
								break;
							case 3:
								mArray[1] = pow( mArray[1] , mArray[2] );
								for( int i = 2; i != 5; i++ )
								{
									mArray[i] = 0;
								}
								count --;
								break;
							case 2:
								mArray[0] = pow( mArray[0] , mArray[1] );
								for( int i = 1; i != 5; i++ )
								{
									mArray[i] = 0;
								}
								count --;
								break;
							default :
								// error
								isSyntaxError = true;
								cerr<<"\n( error : more than 5 numbers or less than 2 numbers . )";
								break;
							}
						}
						wasLastAO = false;
					}
					else
					{
						// set the value of wasAP wasAO
						wasLastAP = false;
						wasLastAO = false;
						
						// if not '_' or '\t'
						if( sym != '.' && sym != ' ' && sym != '\t' && sym !='\n' && sym != -1 )
						{
							isSyntaxError = true;
							
							cerr<<"\n( error : may be othan characters . )";
						}
					}
				}
			}
		}
	}
	

	// ===============================
	// pause when debugging
	//string exit;
	//cin>>exit;
	system("pause");
	// ===============================
	return 0;
}