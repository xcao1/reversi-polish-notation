// ===========================
// Reversi Polish Notation
// Xinlei Cao
// CS570
// ===========================

// head files
#include <iostream>
#include <fstream>
#include <string>
#include <cctype>
#include <cmath>
// using namespace
using namespace std;




// ============================================
// main function
int main()
{
	// ========================================
	// open file
	// store the input file path into a string
	string inputFilePath;
	cout<<"Please enter the path to the input file :"<<endl;
	cin>>inputFilePath; 

	cout<<"\nStore \""<<inputFilePath<<"\" as path to file.\n"<<endl;

	// open the file with the path
	ifstream mInFile(inputFilePath.c_str());
	if( mInFile.fail())
	{
		cout<<"Can not find the file with the given path:\n"<<inputFilePath<< endl;	
	}
	else
	{
		cout<<"Open file successfully with the given path:\n"<<inputFilePath<<endl;

		// ===============================
		// Reversi Polish Notation
		// used to get every character from file
		char sym;
		// store not more than three numbers
		int a = 0, b = 0, c = 0;
		// count which number to store the digit
		int count = 0;
		// if the last character is a digit,default value is false
		bool wasLastADigit = false;
		// if syntax error
		bool isSyntaxError = false;
		// if pow operator
		bool wasLastAP = false, wasLastAO = false;
		// read until end-of-file
	
	
		while( !mInFile.eof() )
		{
			// read every character from file
			sym = mInFile.get();
		
			// the count should be in range 0 - 3
			if( count < 0 || count > 3 )
				cerr<<"\n(error:count should not smaller than 0 or bigger than 3)";
			// store every number
			if( isdigit( sym ) )
			{// if the character readed is a digit
				wasLastADigit = true;
				// store the digit into right number
				int digit = sym - '0';
				switch( count )
				{
				case 0:// if there is no number
					a = a * 10 + digit;
					break;
				case 1:
					b = b * 10 + digit;
					break;
				case 2:
					c = c * 10 + digit;
					break;
				default:
					// error
					isSyntaxError = true;
					cerr<<"\n(error : count should only be 0,1,2,here .)";
					break;
				}
			}
			else
			{
				// set the count , reset wasLastADigit
				if( wasLastADigit == true )
					count++;
				if( count > 3 )
					cerr<<"\n(error : count should not bigger than 3 .)";
				wasLastADigit = false;

				// if reach the end of line
				if( sym == '\n' )
				{// if it is the end of the line
					// print the result
					if( isSyntaxError == true )
						cout<<"\nSYNTAX ERROR"<<endl;
					else
					{
						if( count == 1 )
							cout<<"\nResult :"<<a<<endl;
						else if( count == 0 )
							cout<<"\n";
						else
						{
							cerr<<"\n( error : while reach the end of the line,the count should either be 0 or 1 .)";
							cout<<"\nSYNTAX ERROR"<<endl;
						}
					}// reset the formulation
					count = 0;
					a = 0;
					b = 0;
					c = 0;
					wasLastADigit = false;
					isSyntaxError = false;
					wasLastAP = false;
					wasLastAO = false;
				}
				// =========================================
				// calculation with operators
				// +
				if( sym == '+' )
				{
					switch( count )
					{
					case 3:
						b = b + c;
						c = 0;
						count --;// 3 number,no more numbers ?
						break;
					case 2:
						a = a + b;
						b = 0;
						count --;
						break;
					default :
						// error
						isSyntaxError = true;
						cerr<<"\n( error : more than 3 numbers or less than 2 numbers . )";
						break;
					}
				}
				else if( sym == '-' )
				{
					switch( count )
					{
					case 3:
						b = b - c;
						c = 0;
						count --;
						break;
					case 2:
						a = a - b;
						b = 0;
						count --;
						break;
					default :
						isSyntaxError = true;
						cerr<<"\n( error : more than 3 numbers or less than 2 numbers . )";
						break;
					}
				}
				else if( sym == '*' )
				{
					switch( count )
					{
					case 3:
						b = b * c;
						c = 0;
						count --;
						break;
					case 2:
						a = a * b;
						b = 0;
						count --;
						break;
					default :
						isSyntaxError = true;
						cerr<<"\n( error : more than 3 numbers or less than 2 numbers . )";
						break;
					}
				}
				else if( sym == '/' )
				{
					switch( count )
					{
					case 3:
						b = b / c;
						c = 0;
						count --;
						break;
					case 2:
						a = a / b;
						b = 0;
						count --;
						break;
					default:
						isSyntaxError = true;
						cerr<<"\n( error : more than 3 numbers or less than 2 numbers . )";
						break;
					}
				}
				else if( sym == '%' )
				{
					switch( count )
					{
					case 3:
						b = b % c;
						c = 0;
						count --;
						break;
					case 2:
						a = a % b;
						b = 0;
						count --;
						break;
					default:
						isSyntaxError = true;
						cerr<<"\n( error : more than 3 numbers or less than 2 numbers . ) ";
						break;
					}
				}
				else // pow or '_', '\t','\n',eof or error
				{
					// pow
					if( sym == 'p' )
					{
						wasLastAP = true;
					}
					else if( sym == 'o' )
					{
						if( wasLastAP == true )
							wasLastAO = true;
						wasLastAP = false;
					}
					else if( sym == 'w' )
					{
						if( wasLastAO == true )
						{// the pow operator
							switch( count )
							{
							case 3:
								b = pow( ( float ) b, c );
								c = 0;
								count --;
								break;
							case 2:
								a = pow( ( float ) a, b );
								b = 0;
								count --;
								break;
							default:
								//error
								isSyntaxError = true;
								cerr<<"\n( error : less than 2 numbers or more than 3 numbers . )";
								break;
							}
						}
						wasLastAO = false;
					}
					else
					{
						// set the value of wasAP wasAO
						wasLastAP = false;
						wasLastAO = false;
						
						// if not '_' or '\t'
						if( sym != ' ' && sym != '\t' && sym !='\n' && sym != -1 )
						{
							isSyntaxError = true;
							
							cerr<<"\n( error : not white space or table tabs , may be othan characters . )";
						}
					}
				}
			}
		
	}
	}
	

	// ===============================
	// pause when debugging
	//string exit;
	//cin>>exit;
	system("pause");
	// ===============================
	return 0;
}